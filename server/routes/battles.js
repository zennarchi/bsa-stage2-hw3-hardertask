const fs = require('fs');

var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
  var rawdata = fs.readFileSync('resources/api/battles.json');
  var battles = JSON.parse(rawdata);

  if (battles.length === 0)
    res.status(500).send('There are no elements in the array...');
  else
    res.send(battles);
});

router.post('/create', function (req, res, next) {
  var rawdata = fs.readFileSync('resources/api/battles.json');
  var battles = JSON.parse(rawdata);

  let battle = req.body.battle;
  //res.status(500).send(battle);
  if (battle) {
    battles.push(JSON.parse(battle));
    fs.writeFileSync("resources/api/battles.json", JSON.stringify(battles));
    var rawdata = fs.readFileSync('resources/api/battles.json');
    var battles = JSON.parse(rawdata);
    res.send(battles);
  } else {
    res.status(500).send('Error with creating new fighter');
  }

});


module.exports = router;

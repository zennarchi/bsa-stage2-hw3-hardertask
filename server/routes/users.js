const fs = require('fs');


var express = require('express');
var router = express.Router();



/* GET users listing. */
router.get('/', function (req, res, next) {
  var rawdata = fs.readFileSync('resources/api/fighters.json');
  var fighters = JSON.parse(rawdata);

  if (fighters.length === 0)
    res.status(500).send('There are no elements in the array...');
  else
    res.send(fighters);
});


router.get('/:id', function (req, res, next) {
  var rawdata = fs.readFileSync('resources/api/fighters.json');
  var fighters = JSON.parse(rawdata);
  let parameters = req.params.id - 1;
  if (fighters[parameters])
    res.send(fighters[parameters]);
  else {
    res.status(500).send('There is no fighter with that id');
  }
});


router.get('/details/:id', function (req, res, next) {
  if (req.params.id) {
    var rawdata = fs.readFileSync(`resources/api/details/fighter/${req.params.id}.json`);
    var fighter = JSON.parse(rawdata);
    let parameters = req.params.id - 1;
    if (fighter)
      res.send(fighter);
    else {
      res.status(500).send('There is no fighter with that id');
    }
  } else {
    res.status(500).send('There is no fighter with that id...');
  }
});



// router.post('/create', function (req, res, next) {
//   let user = req.body.user;
//   if (user) {
//     fighters.push(user);
//     res.send(fighters);
//   } else {
//     res.status(500).send('Error with creating new fighter');
//   }

// });

// router.put('/:id', function (req, res, next) {
//   let user_id = req.params.id;
//   var item = fighters.find(item => item.id == user_id);
//   let newData = req.body.user;
//   if (item && newData) {
//     item.id = newData.id;
//     item.name = newData.name;
//     res.send(fighters);
//   } else {
//     res.status(500).send('Error with finding user to change or with new data');
//   }
// });

// router.delete('/:id', function (req, res, next) {
//   let user_id = req.params.id;
//   var item = fighters.find(item => item._id == user_id);
//   //res.status(500).send(item);
//   if (item) {


//     fighters.splice(item._id - 1, 1);
//     res.send(fighters);
//   } else {
//     res.status(500).send('There is no user with that id');
//   }
// });

module.exports = router;

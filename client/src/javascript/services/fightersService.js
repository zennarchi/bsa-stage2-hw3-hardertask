import { callApi, updateClient } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'user';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try {
      const endpoint = `user/details/${_id}`;
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async sendBattleLog(id, fighter1, fighter2, winner) {
    try {
      const endpoint = 'battle/create';
      let battle = { id, fighter1, fighter2, winner };
      const apiResult = await updateClient(endpoint, battle);

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

}

export const fighterService = new FighterService();

const axios = require('axios');
const API_URL = 'https://bsa-stage3.herokuapp.com/';
var proxyUrl = 'https://cors-anywhere.herokuapp.com/';

function callApi(endpoind, method) {
  const url = API_URL + endpoind;
  const options = {
    method
  };

  return fetch(proxyUrl + url, options)
    .then(response =>
      response.ok ? response.json() : Promise.reject(Error('Failed to load'))
    )
    .catch(error => {
      throw error;
    });
}

function updateClient(endpoind, postData) {
  const url = API_URL + endpoind;
  axios.post(proxyUrl + url, {
    battle: JSON.stringify(postData)
  })
    .then((res) => {
      console.log(`statusCode: ${res.statusCode}`)
      console.log(res)
    })
    .catch((error) => {
      console.error(error)
    })

}


export { callApi, updateClient }
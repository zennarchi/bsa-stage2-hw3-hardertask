import View from './view';

class Modal extends View {
    constructor(fighter, handleSave) {
        super();

        this.createModal(fighter, handleSave);
    }

    createModal(fighter, handleSave) {
        const { name } = fighter;
        const nameElement = this.createName(name);

        this.element = this.createElement({ tagName: 'div', className: 'modal' });
        const modalContent = this.createElement({ tagName: 'div', className: 'modal-content' });
        const closeBtn = this.createElement({ tagName: 'span', className: 'close' });
        closeBtn.innerText = "close";
        modalContent.append(closeBtn);
        modalContent.append(nameElement);
        modalContent.append(this.createDetails(fighter, 'health'));
        modalContent.append(this.createDetails(fighter, 'attack'));
        modalContent.append(this.createDetails(fighter, 'defense'));
        modalContent.append(this.createBtn(handleSave, fighter));
        this.element.append(modalContent);
        closeBtn.onclick = () => {
            this.element.style.display = "none";
        }
        return this;
    }

    createName(name) {
        const nameElement = this.createElement({ tagName: 'h2', className: 'test' });
        nameElement.innerText = name;

        return nameElement;
    }

    createDetails(fighter, detailName) {
        const detail = this.createElement({ tagName: 'p', className: 'details' });
        detail.innerText = detailName.toUpperCase() + ': ';
        const detailInput = this.createElement({ tagName: 'input', className: 'form-control' });
        detailInput.value = fighter[detailName];
        detailInput.type = 'number';
        detailInput.id = detailName;
        detail.append(detailInput);
        return detail;
    }

    createBtn(handleSave, fighter) {
        const btn = this.createElement({ tagName: 'button', className: 'btn' });
        btn.innerText = 'Save';
        btn.addEventListener('click', event => handleSave(event, fighter, 3, 4, 5), false);
        return btn;
    }
}

export default Modal;
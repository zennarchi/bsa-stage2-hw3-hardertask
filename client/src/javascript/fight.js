import View from './view';
import FightersView from './fightersView';

class Fight extends View {
    constructor(fighters) {
        super();
        this.createFight(fighters);
    }

    createFight(fighters) {

        console.log(fighters);
        this.element = this.createElement({ tagName: 'div', className: 'fight' });
        const select1 = this.createElement({ tagName: 'select', className: 'select' });
        let option1 = this.createElement({ tagName: 'option', className: 'option' });
        option1.innerText = 'test';
        select1.append(option1);
        this.element.append(select1);

        return this;
    }

    createName(name) {
        const nameElement = this.createElement({ tagName: 'h2', className: 'test' });
        nameElement.innerText = name;

        return nameElement;
    }

    createDetails(fighter, detailName) {
        const detail = this.createElement({ tagName: 'p', className: 'details' });
        detail.innerText = detailName.toUpperCase() + ': ';
        const detailInput = this.createElement({ tagName: 'input', className: 'form-control' });
        detailInput.value = fighter[detailName];
        detailInput.type = 'number';
        detailInput.id = detailName;
        detail.append(detailInput);
        return detail;
    }

    createBtn(handleSave, fighter) {
        const btn = this.createElement({ tagName: 'button', className: 'btn' });
        btn.innerText = 'Save';
        btn.addEventListener('click', event => handleSave(event, fighter, 3, 4, 5), false);
        return btn;
    }
}

export default Fight;
import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import Fighter from './fighter';
import Fight from './fight';

class App {
  constructor() {
    this.startApp();
    //this.showFightChoose();
    let fighter1 = new Fighter('Artem', 10, 3, 1);
    let fighter2 = new Fighter('Vasya', 5, 3, 2);
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');


  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;



      App.rootElement.append(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }



}

export default App;